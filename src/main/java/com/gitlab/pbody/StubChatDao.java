package com.gitlab.pbody;

public class StubChatDao implements ChatDao {
    private String lastMessage;

    public String getLastMessage() {
        return this.lastMessage;
    }

    @Override
    public void sendMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }
}
