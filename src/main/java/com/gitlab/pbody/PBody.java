package com.gitlab.pbody;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;

public class PBody {
    public static final String SPLIT = " ";
    private List<Object> bots = new ArrayList<>();
    private Map<Type, ArgumentParser> parsers = new HashMap<>();

    public PBody() {
        parsers.put(Integer.class, new IntegerParser());
        parsers.put(int.class, new IntegerParser());
        parsers.put(Double.class, new DoubleParser());
        parsers.put(double.class, new DoubleParser());
        parsers.put(Float.class, new FloatParser());
        parsers.put(float.class, new FloatParser());
    }

    public void registerBot(Object bot) {
        this.bots.add(bot);
    }

    public void fireMessage(String lastMessage) {
        String[] split = lastMessage.split(SPLIT);

        if (split[0].equals("poll")) {
            String createBreakpoint = "";
        }

        try {
            for (Object bot : this.bots) {
                Bot botClass = (Bot) bot.getClass().getAnnotations()[0];

                if (split[0].equals(botClass.value())) {
                    for (Method method : bot.getClass().getMethods()) {
                        for (Annotation annotation : method.getAnnotations()) {
                            Command command = (Command) annotation;

                            if (split[1].equals(command.value()) || "".equals(command.value())) {
                                if (method.getParameterCount() == 0) {
                                    method.invoke(bot);
                                } else {
                                    List<Object> args = new ArrayList<>();

                                    int i = "".equals(command.value()) ? 1 : 2;

                                    Parameter[] parameters = method.getParameters();

                                    for (int x = 0; x < parameters.length; x++) {
                                        Parameter param = parameters[x];
                                        Class<?> paramType = param.getType();

                                        if (parsers.containsKey(paramType)) {
                                            args.add(parsers.get(paramType).parse(split[i]));
                                        } else if (x + 1 == parameters.length) {
                                            args.add(String.join(SPLIT, Arrays.copyOfRange(split, i, split.length)));
                                        } else {
                                            args.add(split[i]);
                                        }

                                        i++;
                                    }

                                    method.invoke(bot, args.toArray());
                                }
                            }
                        }
                    }
                }
            }
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException("This should never happen", e);
        }
    }

    public void registerArgumentParser(ArgumentParser userParser) {
        for (Type type : userParser.getClass().getGenericInterfaces()) {
            if (type instanceof ParameterizedType) {
                ParameterizedType parameterizedType = (ParameterizedType) type;
                Type typeArg = parameterizedType.getActualTypeArguments()[0];

                parsers.put(typeArg, userParser);
            }
        }
    }
}
