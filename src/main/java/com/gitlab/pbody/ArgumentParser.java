package com.gitlab.pbody;

public interface ArgumentParser<T> {
    T parse(String name);
}
