package com.gitlab.pbody;

public class IntegerParser implements ArgumentParser<Integer> {
    @Override
    public Integer parse(String name) {
        return Integer.parseInt(name);
    }
}
