package com.gitlab.pbody;

public class FloatParser implements ArgumentParser<Float> {
    @Override
    public Float parse(String name) {
        return Float.parseFloat(name);
    }
}
