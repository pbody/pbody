package com.gitlab.pbody;

public class DoubleParser implements ArgumentParser<Double> {
    @Override
    public Double parse(String name) {
        return Double.parseDouble(name);
    }
}
