package com.gitlab.pbody;

public interface ChatDao {
    void sendMessage(String lastMessage);
}
