package com.gitlab.pbody;

import com.greghaskins.spectrum.Spectrum;
import com.greghaskins.spectrum.Variable;
import org.junit.runner.RunWith;

import static com.greghaskins.spectrum.Spectrum.*;
import static org.junit.Assert.*;

@RunWith(Spectrum.class)
public class TestPBody {
    {
        describe("PBody", () -> {
            Variable<PBody> pbody = new Variable<>();
            beforeEach(() -> {
                pbody.set(new PBody());
            });

            describe("Scripts", () -> {
                it("should dispatch messages to bots", () -> {
                    Variable<Boolean> wasExecuted = new Variable<>(false);

                    @Bot("foodbot")
                    class FoodBot {
                        @Command("menu")
                        public void method() {
                            wasExecuted.set(true);
                        }
                    }

                    pbody.get().registerBot(new FoodBot());

                    pbody.get().fireMessage("foodbot menu");
                    assertTrue("Method was not executed", wasExecuted.get());
                });

                it("should distinguish between different operations on bots", () -> {
                    Variable<String> message = new Variable<>();

                    @Bot("foodbot")
                    class BasicBot {
                        @Command("menu")
                        public void method() {
                            message.set("method was executed");
                        }

                        @Command("order")
                        public void command() {
                            message.set("command was executed");
                        }
                    }

                    pbody.get().registerBot(new BasicBot());

                    pbody.get().fireMessage("foodbot menu");
                    assertEquals("method was executed", message.get());

                    pbody.get().fireMessage("foodbot order");
                    assertEquals("command was executed", message.get());
                });

                it("should allow parameters to be injected", () -> {
                    Variable<String> argument = new Variable<>();

                    @Bot("foodbot")
                    class BasicBot {
                        @Command("order")
                        public void method(@Argument String item) {
                            argument.set(item);
                        }
                    }

                    pbody.get().registerBot(new BasicBot());

                    pbody.get().fireMessage("foodbot order cheeseburgers");
                    assertEquals("cheeseburgers", argument.get());
                });

                it("should allow multiple parameters to be injected", () -> {
                    Variable<String> restaurant = new Variable<>();
                    Variable<String> item = new Variable<>();

                    @Bot("foodbot")
                    class BasicBot {
                        @Command("order")
                        public void method(@Argument String restaurant_, @Argument String item_) {
                            restaurant.set(restaurant_);
                            item.set(item_);
                        }
                    }

                    pbody.get().registerBot(new BasicBot());

                    pbody.get().fireMessage("foodbot order mcsteveys cheeseburgers");
                    assertEquals("mcsteveys", restaurant.get());
                    assertEquals("cheeseburgers", item.get());
                });

                it("should match the correct bot", () -> {
                    Variable<String> lastMessage = new Variable<>();

                    @Bot("foodbot")
                    class FoodBot {
                        @Command("order")
                        public void order() {
                            lastMessage.set("ordering food");
                        }
                    }

                    @Bot("bookbot")
                    class TimeBot {
                        @Command("order")
                        public void time() {
                            lastMessage.set("books ordered");
                        }
                    }

                    pbody.get().registerBot(new FoodBot());
                    pbody.get().registerBot(new TimeBot());

                    pbody.get().fireMessage("foodbot order");
                    assertEquals("ordering food", lastMessage.get());

                    pbody.get().fireMessage("bookbot order");
                    assertEquals("books ordered", lastMessage.get());
                });

                describe("argument parsing", () -> {
                    it("should parse object integers", () -> {
                        Variable<Integer> integer = new Variable<>();

                        @Bot("foodbot")
                        class FoodBot {
                            @Command("order")
                            public void time(@Argument Integer numberOf, @Argument String item) {
                                integer.set(numberOf);
                            }
                        }

                        pbody.get().registerBot(new FoodBot());
                        pbody.get().fireMessage("foodbot order 4 cheeseburgers");

                        assertEquals((Integer) 4, integer.get());
                    });

                    it("should parse primative integers", () -> {
                        Variable<Integer> integer = new Variable<>();

                        @Bot("foodbot")
                        class FoodBot {
                            @Command("order")
                            public void time(@Argument int numberOf, @Argument String item) {
                                integer.set(numberOf);
                            }
                        }

                        pbody.get().registerBot(new FoodBot());
                        pbody.get().fireMessage("foodbot order 4 cheeseburgers");

                        assertEquals((Integer) 4, integer.get());
                    });

                    it("should parse object doubles", () -> {
                        Variable<Double> amt = new Variable<>();

                        @Bot("paybot")
                        class PayBot {
                            @Command("pay")
                            public void time(@Argument String person, @Argument Double amount) {
                                amt.set(amount);
                            }
                        }

                        pbody.get().registerBot(new PayBot());
                        pbody.get().fireMessage("paybot pay kyle 0.01");

                        assertEquals((Double) 0.01, amt.get());
                    });

                    it("should parse primative doubles", () -> {
                        Variable<Double> amt = new Variable<>();

                        @Bot("paybot")
                        class PayBot {
                            @Command("pay")
                            public void time(@Argument String person, @Argument double amount) {
                                amt.set(amount);
                            }
                        }

                        pbody.get().registerBot(new PayBot());
                        pbody.get().fireMessage("paybot pay kyle 0.01");

                        assertEquals((Double) 0.01, amt.get());
                    });

                    it("should parse object floats", () -> {
                        Variable<Float> amt = new Variable<>();

                        @Bot("paybot")
                        class PayBot {
                            @Command("pay")
                            public void time(@Argument String person, @Argument Float amount) {
                                amt.set(amount);
                            }
                        }

                        pbody.get().registerBot(new PayBot());
                        pbody.get().fireMessage("paybot pay kyle 0.01");

                        assertEquals((Float) 0.01F, amt.get());
                    });

                    it("should parse primative floats", () -> {
                        Variable<Float> amt = new Variable<>();

                        @Bot("paybot")
                        class PayBot {
                            @Command("pay")
                            public void time(@Argument String person, @Argument float amount) {
                                amt.set(amount);
                            }
                        }

                        pbody.get().registerBot(new PayBot());
                        pbody.get().fireMessage("paybot pay kyle 0.01");

                        assertEquals((Float) 0.01F, amt.get());
                    });

                    it("should allow custom argument parsers to be registered", () -> {
                        class User {
                            public final String name;

                            public User(String name) {
                                this.name = name;
                            }
                        }

                        Variable<User> userVariable = new Variable<>();

                        class UserParser implements ArgumentParser<User> {
                            @Override
                            public User parse(String name) {
                                return new User(name);
                            }
                        }

                        pbody.get().registerArgumentParser(new UserParser());

                        @Bot("user")
                        class UserBot {
                            @Command("add")
                            public void add(User user) {
                                userVariable.set(user);
                            }
                        }

                        pbody.get().registerBot(new UserBot());
                        pbody.get().fireMessage("user add trashvis");

                        assertEquals("trashvis", userVariable.get().name);
                    });

                    it("should parse simple commands", () -> {
                        Variable<String> lastMessage = new Variable<>();

                        @Bot("echo")
                        class EchoBot {
                            @Command("")
                            public void echo(@Argument String echoString) {
                                lastMessage.set(echoString);
                            }
                        }

                        pbody.get().registerBot(new EchoBot());
                        pbody.get().fireMessage("echo hello");

                        assertEquals("hello", lastMessage.get());
                    });

                    it("should parse multi word strings", () -> {
                        Variable<String> complexOutput = new Variable<>();

                        @Bot("poll")
                        class PollBot {
                            @Command("create")
                            public void create(@Argument String pollName) {
                                complexOutput.set(pollName);
                            }
                        }

                        pbody.get().registerBot(new PollBot());
                        pbody.get().fireMessage("poll create \"Who is the best developer?\"");

                        assertEquals("\"Who is the best developer?\"", complexOutput.get());
                    });
                });
            });
        });
    }
}
